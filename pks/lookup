#!/usr/bin/perl -wT

#
# Simple HKP lookup script for debian keys.  Accepts queries of the
# form:
#
#     GET /pks/lookup?op=get&search=0x<id> HTTP/1.0
#
# Copyright (C) 2000 Brendan O'Dea <bod@debian.org>
# Copying policy: GPL (https://www.gnu.org/copyleft/gpl.html)
#

use strict;
use CGI;

my $conf    = do '/srv/keyring.debian.org/keyserver.conf';
my $query   = CGI->new;
my $op      = $query->param('op');
my $search  = $query->param('search');
my $title   = 'Public Key Server -- Error';
my $status  = '200 OK';
my $content;

if (defined $op and $op eq 'get' and
    defined $search and $search =~ /^0x[\da-f]{0,24}([\da-f]{8,16})$/i)
{
    my $key = "0x".$1;
    my $keyrings = join " ", map("--keyring=$conf->{CURRENT}/$_",
                                 @{$conf->{KEYNAME}});
    $_ = `$conf->{GPG} $keyrings \\
		       --keyring=$conf->{MASTER}/$conf->{ROLEKEY}.gpg \\
                       --lock-never --no-default-keyring \\
                       --no-permission-warning --secret-keyring /dev/null \\
		       --armor --export --comment='Key ID: $key' $key 2>&1`;

    if (/PUBLIC KEY BLOCK/)
    {
	s/&/&amp;/g;
	s/</&lt;/g;
	s/>/&gt;/g;

	$title = "Public Key Server -- Get ``$key''";
	$content = $query->pre("\n" . $_);
    }
    else
    {
	$content = $query->p('No matching keys in database.');
    }
}
else
{
    # RFC defines we should answer to index queries with 501 if not
    # implemented (thanks to Daniel Kahn Gillmor - RT#2220)
    # https://tools.ietf.org/html/draft-shaw-openpgp-hkp-00#section-3.1.2.2
    $status = (defined $op and defined $search) ?
	'501 not implemented (only "get" command available)' :
	'406 "op" and "search" keywords required';
    $content = $query->p(<<EOT);
Invalid query.  Only requests of the form "op=get&search=0x&lt;id&gt;"
are supported.
EOT
}

print $query->header(-status => $status),
      $query->start_html($title),
      $query->h1($title),
      $content,
      $query->end_html;

exit;
